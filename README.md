# Sharetool

Small tool to slit up costs from bundles

## Data format

The entry data are located in the `data` directory.

The file `people.csv` looks like that :

```csv
John Doe,5,1258.12
```
