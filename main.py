from people import *
from share import Share

if __name__ == "__main__":
    people = People()
    with open('data/people.csv', newline='') as csvfile:
        people.parseCSVstr(csvfile)
    Share.calc_repartition(people)
    people.print()

