from people import *

class Share:
    def calc_repartition(people):
        total_income = 0
        total_claims = 0
        total_index = 0

        # compute sums
        for person in people:
            total_income += person.income
            total_claims += person.claims

        #compute temp indexes and total index
        for person in people:
            p_claims = person.claims/total_claims
            p_income = person.income/total_income
            person.index = p_claims * p_income
            total_index += person.index
            
        # compute real index
        for person in people:
            person.index = person.index / total_index

